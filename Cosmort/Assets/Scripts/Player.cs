﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Player : MonoBehaviour, IPointerDownHandler,     IPointerUpHandler
{
    //how long we've been holding down the button, -1 means we are not holding it down
    public float TimeDown;

    //the prefab of the drop particle
    public GameObject ProjectilePrefab;
    
    //the orbiting object parent
    public GameObject Orbit;
    
    //the particle that the player sees    
    public GameObject Particle;

    //rotation speed of the orbit
    public float OrbitSpeed;

    //how fast the orbit slows to a stop
    public float SlowdownTime;

    //the speed of the button when the palyer lets go
    public float BounceForce;

    //the base size of the drop particle
    public Vector3 BaseSizeParticle;

    //the final size of the drop particle, up to orbit object size
    public Vector3 MaxSizeParticle;

    //instance of drop particle
    private GameObject ProjectileInstance;

    //rotation speed of the player particle
    public float ParticleRotateSpeed;

    //the orbiting particle will accelerate spinning up to this speed when its stopped
    public float MaxOrbitRotationSpeed;

    //properties of the orbiting star objeect
    private HU_Star _orbitProperties;


    /// <summary>
    /// Mouse pressed down start recording time and slowing down the orbit
    /// </summary>
    /// <param name="eventData"></param>
    public void OnPointerDown(PointerEventData eventData)
    {
        TimeDown = 0;
        ProjectileInstance = GameObject.Instantiate(ProjectilePrefab, Orbit.transform.position, Orbit.transform.rotation);
        ProjectileInstance.transform.localScale = BaseSizeParticle;
    }

    /// <summary>
    /// Mouse let go, resume orbit
    /// </summary>
    /// <param name="eventData"></param>
    public void OnPointerUp(PointerEventData eventData)
    {
        float duration = TimeDown;
        TimeDown = -1.0f;

        //after letting go then apply force in opposite direction of the orbit
        Vector2 dir = Orbit.transform.position - transform.position;
        dir = -dir.normalized;
        GetComponent<Rigidbody2D>().velocity += dir * BounceForce;
    }

    // Update is called once per frame
    void Update()
    {

        float orbitSpeed = OrbitSpeed;

        //the particle rotates horizontally and slightly up
        Particle.transform.RotateAround(this.transform.position, Vector3.up, ParticleRotateSpeed * Time.deltaTime);
        Particle.transform.RotateAround(this.transform.position, Vector3.left, ParticleRotateSpeed/4 * Time.deltaTime);       


        //if button is being pressed
        if (TimeDown >= 0)
        {
            //gradually reduce the rotation speed of the orbiting ball
            orbitSpeed = Mathf.Lerp(OrbitSpeed, 0, TimeDown/SlowdownTime);

            //the orbit particle accelerates
            float orbitRotSpeed = Mathf.Lerp(ParticleRotateSpeed, MaxOrbitRotationSpeed, TimeDown / SlowdownTime);

            TimeDown += Time.deltaTime;

            //drop a little particle with a scale based on how long you've been holding down the button and at the position of the orbit
            ProjectileInstance.transform.localScale = Vector3.Lerp(BaseSizeParticle, MaxSizeParticle, TimeDown / SlowdownTime);
            ProjectileInstance.transform.position = Orbit.transform.position;

            //if the player is holding down the button we want the spinning to accelerate
            Orbit.transform.RotateAround(Orbit.transform.position, Vector3.up, orbitRotSpeed * Time.deltaTime);
            Orbit.transform.RotateAround(Orbit.transform.position, Vector3.left, orbitRotSpeed * Time.deltaTime);
        }

        else
        {
            //the orbiting particle rotates a bit as well
            Orbit.transform.RotateAround(Orbit.transform.position, Vector3.up, OrbitSpeed * Time.deltaTime);
            Orbit.transform.RotateAround(Orbit.transform.position, Vector3.left, OrbitSpeed * Time.deltaTime);
        }
        


        //rotate around the main player particle
        Orbit.transform.RotateAround(this.transform.position, Vector3.forward, orbitSpeed * Time.deltaTime);

    }
}

